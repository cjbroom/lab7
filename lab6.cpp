/*
Name:		 Jacob Broom
Course:		 CPSC 1021, 001, F20
Username:	 cjbroom@g.clemson.edu
Lab #:		 6
TA:			 Nushrat Humaira
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}

int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  cout << "\nHello, welcome to lab 6, C++ Intro\n" << endl;

  /* Create an array of 10 employees and fill information from standard input with prompt messages */
  employee initialArr[10];
  
  for (int i = 0; i < 10; ++i)
  {
	  cout << "Please enter the employee's last name " << endl;
	  cin >> initialArr[i].lastName;
	  cout << "Please enter the employee's first name " << endl;
	  cin >> initialArr[i].firstName;
	  cout << "Please enter the employee's birth year " << endl;
	  cin >> initialArr[i].birthYear;
	  cout << "Please enter the employee's hourly wage " << endl;
	  cin >> initialArr[i].hourlyWage;
	  cout << "\n" << endl;
  }

  /* After the array is created and initialzed we call random_shuffle() see the notes to determine the parameters to pass in. */
  /* This is to seed the random generator */
  srand(unsigned (time(0)));
  random_shuffle(initialArr, initialArr + 10, myrandom);

  /* Test array for debugging purposes
	cout << "The shuffled original array: " << endl;
  for (int i = 0; i < 10; ++i)
  {	  
	  cout << initialArr[i].lastName << ", " << initialArr[i].firstName << endl;
	  cout << initialArr[i].birthYear << endl;
	  cout << initialArr[i].hourlyWage << endl;
	  cout << "\n";
  }
  */
   /* Build a smaller array of 5 employees from the first five cards of the array created above */
	employee newArray[5];

  for (int i = 0; i < 5; ++i)
  {		
	  newArray[i].lastName = initialArr[i].lastName;
	  newArray[i].firstName = initialArr[i].firstName;
	  newArray[i].birthYear = initialArr[i].birthYear;
	  newArray[i].hourlyWage = initialArr[i].hourlyWage;
  }

  /* Test array for debugging purposes
	cout << "The shuffled new array: " << endl;
  for (int i = 0; i < 5; ++i)
  {
	  cout << newArray[i].lastName << ", " << newArray[i].firstName << endl;
	  cout << newArray[i].birthYear << endl;
	  cout << newArray[i].hourlyWage << endl;
	  cout << "\n";
  }
  */
   /* Sort the new array. (last name) Links to how to call this function is in the specs provided */
	sort(newArray, newArray + 5, name_order);

   /* Test array for debugging purposes
	cout << "The sorted and shuffled new array: " << endl;
	for (int i = 0; i < 5; ++i)
	{
		cout << newArray[i].lastName << ", " << newArray[i].firstName << endl;
		cout << newArray[i].birthYear << endl;
		cout << newArray[i].hourlyWage << endl;
		cout << "\n";
	}
	*/
	/* Now print the array below */
	// Printing the sorted, shufffled, range based for loop with formatting
	cout << "The sorted and shuffled new array, formatted as specified: " << endl;
	// int i;
	for (auto i : newArray)
	{
		cout << std::setw(15) << std::right << i.lastName << ", " << i.firstName << endl;
		cout << std::setw(15) << std::right << i.birthYear << endl;
		cout << std::setw(15) << std::right << fixed << showpoint << setprecision(2) << i.hourlyWage << endl;
		cout << endl;
	}

  return 0;
}

// This function will be passed to the sort funtion. Hints on how to implement this is in the specifications document. */
	bool name_order(const employee& lhs, const employee& rhs) 
	{
//	IMPLEMENT
	bool left = true;
	bool right = false;

		if (lhs.lastName < rhs.lastName)
		{
			return left;
		}
		else 
		{
			return right;
		}
	}

